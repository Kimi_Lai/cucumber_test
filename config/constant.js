const appRoot = require("app-root-path");
const config = require(appRoot + "/config/config.js")();

module.exports = function () {
	return {
		mongoUrl: "mongodb://" + encodeURIComponent(config.mongodb.user) + ":" + encodeURIComponent(config.mongodb.password) + "@" + config.mongodb.url,
	};
};