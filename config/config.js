module.exports = function () {
	return {
		cookieOptions: {
			domain: ".poll.money",
			httpOnly: true,
			secure: true,
			maxAge: 60 * 60 * 1000 // in millisecond
		},
		frontend: {
			url: "dev.poll.money"
		},
		jwt: {
			key: "0hhh... a key. You know, | can... use a key for hours...",
			iss: "api.poll.money"
		},
		mongodb: {
			url: "pbmongo:27017",
			db: "pb",
			user: "dbadmin-pb",
			password: "dbadmin-pass-pb"
		},
		sms: {
			smsInterval: 5 * 60,
			smsAlive: 30 * 60,
			codeLength: 7,
			mitake: {
				baseUrl: "http://smsapi.mitake.com.tw/api/mtk/",
				username: "53091802",
				password: "9yk3tGe8ZYdUtNmp4PW5V7fs",
			}
		},
		reset: {
			tokenAlive: 30 * 60
		},
		register: {
			verifyAlive: 30 * 60
		},
		s3: {
			endPoint: "minio",
			port: 9000,
			useSSL: false,
			accessKey: "AKIA5MVBMOGQKE4MTUWF",
			secretKey: "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",
			bucketName: "polling-block",
			expiry: 15 * 60 // seconds
		},
		hostUrl: "https://api.poll.money",
	};
};