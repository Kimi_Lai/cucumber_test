// const createError = require("http-errors");
const express = require("express");
const appRoot = require("app-root-path");
global.appRoot = appRoot;
var app = express();
const Sentry = require("@sentry/node");
const Tracing = require("@sentry/tracing");

app.set("views", appRoot + "/views");
app.set("view engine", "pug");
Sentry.init({
	dsn: "https://7e8e3c47d90443e5a4c8f6207fa058c4@o567799.ingest.sentry.io/5713683",
	integrations: [
		// enable HTTP calls tracing
		new Sentry.Integrations.Http({ tracing: true }),
		// enable Express.js middleware tracing
		new Tracing.Integrations.Express({ app }),
	],
})

// The error handler must be before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());

// catch 404 and forward to error handler
app.use((req, res, next) => {
	next(createError(404));
});

// error handler
app.use((err, req, res) => {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};
	// render the error page
	if (res.headersSent) {
		return;
	} else {
		res.status(err.status || 500);
		res.json({ ...err, message: err.message });
	}
});

module.exports = app;
