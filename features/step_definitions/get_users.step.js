const { Given, When, Then } = require("@cucumber/cucumber");
const chai = require('chai');
const request = require('supertest');
const appRoot = require("app-root-path");
const app = require(appRoot + "/app");
const MongoModels = require("mongo-models");
const { mongoUrl }  = require(appRoot + "/config/constant");
const config = require(appRoot + "/config/config");
const regeneratorRuntime = require("regenerator-runtime");
chai.should()

const userinfo = {
	"phone": "0912345678",
	"email": "test_mail_XXXXXX@testmail.com",
	"password": "somehashedpassword",
	"nickname": "String",
	"username": "String",
	"bio": "String",
	"gender": "String",
	"birthdayYear": 1,
	"birthdayMonth": 1,
	"birthdayDay": 10,
	"national": "String",
	"city": "String",
	"district": "String",
	"nationalId": "String",
	"following": ["String", "String1", "String2"],
	"beneBankCode": "7001234",
	"beneAccountNumber": "5520123456781234",
};

var connection, db, pbToken;
  
Given('Create Database connection', async () => {
  try {
    const mongoSetting = {
      uri: mongoUrl,
      db: config.mongodb.db,
    };
    const connection = await MongoModels.connect(mongoSetting, { useNewUrlParser: true, useUnifiedTopology: true });
  } catch (e) {
    console.error(e);
  }
});

When('User Verify phone number with SMS cerification code', async () => {
  const smsResponse = await request(app)
      .post("https://api.poll.money/auth/register/verify/phone")
      .send({ "phone": "0912345678", "verificationCode": "1234567"});
  expect(smsResponse.status).toBe(200);
  // dr.findElement(By.id("phone")).sendKeys("0932313332");
});

Then('SMS cerification is successful', function () {
    return smsResponse.body.message;
});


When('User starts to login', async () =>  {
  try{
  const response = await request(app)
				.post("https://api.poll.money/auth/register")
				.send(userinfo);
  
  pbToken = response.body.pbToken;

  expect(response.status).toBe(200);

  const insertedUser = await User.findOne({ email: userinfo.email });
  const responseId = response.body.id;
  const insertedUserId = insertedUser._id;

  expect(insertedUserId.toHexString()).toBe(responseId);
} catch (e) {
  console.error(e);
}
});

Then('Message displayed Login Successfully', function () {
  console.log("Message displayed Login Successfully");
});
       